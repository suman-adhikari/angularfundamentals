# Flow : InputOutput, TemplateReference

## Passing data from child component to parent component with @Output


**Parent Template**
The function name SaveSessionEvent and the @output name from child should should have same name. That's how they are binded.

```typescript
<app-create-session
  *ngIf="IsAdd"
  (SaveSessionEvent)="SaveSession($event)"
  (CancelSessionEvent)="CancelSessionEvent()"
  >Add New Session</app-create-session
>
```

**Parent component** 
It will have to implement the SaveSession($event)

```typescript
SaveSession(session:ISession){
      //save session calling service
   }
```

**Child Template**

```typescript
<form
    [formGroup]="sessionFormGroup"
    (ngSubmit)="SaveSession(sessionFormGroup.value)"
    autocomplete="off"
  >
  ...
  reset of the code
  ...
  </form>
```

**Child component**

 First we create an EventEmitter and decorate with @Output(). 
 Remmber the same name SaveSessionEvent should be used in the parent componet's template to bind to this event.

 ```typescript
@Output() SaveSessionEvent = new EventEmitter();

  SaveSession(formValue:any){ 
    this.SaveSessionEvent.emit(formValue) 
   }
```



## TemplateReference

```typescript
<div>
    <h1>Upcoming Events</h1>
    <hr>
    <!-- the #thumbnail is the template reference variable. 
    Now the eventList(parent) and access EventThumbnail(child)'s property and method
     -->
    <app-event-thumbnail [event]="event1" #thumbnail> </app-event-thumbnail> 
   
    <!-- accessing child's method via  template reference variable -->
    <button (click)="thumbnail.handleClick()">Call Child's method</button> 

    <!-- accessing child's property via  template reference variable -->
   <h2>{{thumbnail.childProperty}}</h2>
    

</div>
```


## Template based Form with validation

```typescript
<div class="col-md-4">
  <form #loginForm="ngForm" (ngSubmit)="login(loginForm.value)" autocomplete="off">
    <div class="form-group">
      <label for="userName">User Name:</label>
      <em *ngIf="loginForm.controls.userName?.invalid && loginForm.controls.userName?.touched">required</em>
      <input id="userName" (ngModel)="(userName)"name="userName" type="text" class="form-control" placeholder="User Name..." required />
    </div>
    <div class="form-group">
      <label for="password">Password:</label>
      <em *ngIf=" loginForm.controls.password?.invalid && loginForm.controls.password?.touched">required</em>
      <input id="password" type="password" (ngModel)="(password)" name="password" class="form-control" placeholder="Password..." required/>
    </div>

    <button type="submit" [disabled]="loginForm.invalid" class="btn btn-primary"> Login </button>
    <button type="button" class="btn btn-default">Cancel</button>
  </form>
</div>
```

```typescript
export class LoginComponent implements OnInit {

  userName!:string
  password!:string

  constructor(private auth:AuthService, private route: Router) { }

  login(formvalues:any){
    this.auth.login(formvalues.userName,formvalues.password);
    this.route.navigate(["/events"]);
  }

  ngOnInit(): void {
  }

}
```



## Reactive form with FormGroup, formControl and multiple validation

```typescript
  <div class="col-md-4">
    <form #updateProfileForm="ngForm"[formGroup]="profileForm" (ngSubmit)="updateProfile(updateProfileForm.value)" autocomplete="off" novalidate>
      <div class="form-group">
        <label for="firstName">First Name:</label>
        <em *ngIf=" !isValidFname() && profileForm.controls.firstName.errors?.required">required</em>
        <em *ngIf="!isValidFname() && profileForm.controls.firstName.errors?.pattern">name must be string</em>
        <input formControlName="firstName"id="firstName" type="text"class="form-control"placeholder="First Name..."/>
      </div>
      <div class="form-group">
        <label for="lastName">Last Name:</label>
        <em *ngIf="!isValidLname()">required</em>
        <input formControlName="lastName"id="lastName"type="text"class="form-control"placeholder="Last Name..."/>
      </div>

      <button type="submit" [disabled]="!isValid()" class="btn btn-primary"> Save</button>
      <button type="button" class="btn btn-default">Cancel</button>
    </form>
  </div>
</div>
```

```typescript
export class ProfileComponent implements OnInit {

  firstName!:FormControl;
  lastName!:FormControl;
  profileForm!: FormGroup;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.firstName = new FormControl(this.auth.currentUser.firstName,[Validators.required, Validators.pattern('[a-zA-Z].*')]);
    this.lastName = new FormControl(this.auth.currentUser.lastName,Validators.required);

    //profileForm this variable should match with template's formGroup ie [formGroup]="profileForm"
    //the key of FormGroup should match with template's formControlName ie formControlName="firstName", formControlName="lastName"
    this.profileForm = new FormGroup({
      firstName:this.firstName,
      lastName:this.lastName
    })

  }  
```

## Reusing components with Content Projection
- To change the content of the component dynamically as per the requirement.

Here the collapsable component is the content projection component. It is passed a title.
And how the content wrapped by this component will be displayed will be handled by the component's template.

```typescript
<div class="row" *ngFor="let session of sessions">
  <div class="col-md-10">
    <collapsable title="{{ session.name }}">
      <h6>{{ session.presenter }}</h6>
      <span>Duration: {{ session.duration }}</span
      ><br />
      <span>Level: {{ session.level }}</span>
      <p>{{ session.abstract }}</p>
    </collapsable>
  </div>
</div>
```

**collapsable component**

<ng-content> : This is the place where the content wrapped by the the component will be placed.
The component defines a directive called <ng-content> where the content wrapped by it will be placed.

Advantage: we can add html or events before and after content wrapped by the component.

```typescript

import { Component, Input } from '@angular/core';

@Component({
  selector:'collapsable',
  template: `<div class="well pointer" (click)="toggle()">
            <h4 class="well-title">{{title}}</h4>
            <ng-content *ngIf="visible"> </ng-content>
           </div> `,

   styles:[ ' .pointer{ cursor:pointer } ' ]
})
export class Collapsable  {

  @Input() title: string = "";
  visible:boolean = false;

  toggle(){
       this.visible = ! this.visible;
   }

}
```


## Multiple slot Content Projection
We can create various section and replace with content wrapped by the component.
Let's modify the previous example to have different sections.
In the template well-title is one section and well-body is another. We will use these two section in the collapsable component.

```typescript
<div class="row" *ngFor="let session of sessions">
  <div class="col-md-10">
    <collapsable>
      <div well-title>
        {{ session.name }}
        <i
          *ngIf="session.voters.length >= 2"
          class="glyphicon glyphicon-fire"
          style="color: red"
        ></i>
      </div>
      <div well-body>
        <h6>{{ session.presenter }}</h6>
        <span>Duration: {{ session.duration }}</span
        ><br />
        <span>Level: {{ session.level }}</span>
        <p>{{ session.abstract }}</p>
      </div>
    </collapsable>
  </div>
</div>
```

**Collapsable component**

The <ng-content> with _well-title_ selector will get the well-title section from the above code and well-body the respective code.

```typescript 
import { Component, Input } from '@angular/core';
@Component({
  selector:'collapsable',
  template: `<div class="well pointer" (click)="toggle()">
            <h4>
               <ng-content select="[well-title]" ></ng-content>
            </h4>
            <ng-content  select="[well-body]" *ngIf="visible"> </ng-content>
           </div> `,

   styles:[ ' .pointer{ cursor:pointer } ' ]
})
export class Collapsable  {
  visible:boolean = false;

  toggle(){
       this.visible = ! this.visible;
   }

}
```


## Pipes
They help transform values. 
ex: new Date() in js returns values in the format Fri Sep 26 2036 00:00:00 GMT+0545 (Nepal Time). This doesnot look good.
Here we can implement pipe to transform or format the date value. Also, we can also provide parameter to pipe to further format as our requirement.

```typescript
<div><strong>Date:</strong> {{ event?.date | date }}</div> // Sep 26, 2036 
<div><strong>Date:</strong> {{ event?.date | date:'short' }}</div> //9/26/36, 12:00 AM
<div><strong>Date:</strong> {{ event?.date | date:'shortDate' }}</div> //9/26/36
<div><strong>Date:</strong> {{ event?.date | date:'shortTime' }}</div> //12:00 AM
<div><strong>Date:</strong> {{ event?.date | date:'y-M-d' }}</div> //2036-9-26
```


Siilarly we have currency pipe to display number in currecny format

` typescript{{  somenumber | currency:'USD' }}`


# Custom Pipe

```typescript
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name:'duration'})
export class DurationPipe implements PipeTransform{

  //transform takes numeric value and returns striing
  transform(value:number): string{
      switch(value){
       case 1 : return 'Half hour';
       case 2 : return 'One hour';
       case 3 : return 'One and half hour';
       case 4 : return 'Two hour';
       default: return value.toString();

     }
  }

}

<span>Duration: {{ session.duration | duration }}</span

```
## Filter 

Useful when we want to filter a list of object based on user selection.
Each button has click event that sets value for filterBy property
This filterBy property is passed to SessionList component by model binding
The component SessionList is responsible for displaying list of sessions

```typescript
 <button [class.active]="filterBy == 'all'" (click)="filterBy = 'all'" class="btn btn-default">All</button>
 <button [class.active]="filterBy == 'beginner'"(click)="filterBy = 'beginner'"class="btn btn-default">Benginner</button>
 <app-session-list [filterBy]="filterBy" *ngIf="!IsAdd" [sessions]="event?.sessions"></app-session-list>
```

SessionList component
In this component, we define a filteredSession to hold the list of sessions based on value of filterBy passed to it.
Any change to @input filterBy will trigger the ngOnChanges() method automatically
This method holds logic for filtering session based on filterBy value.
Finally the template of this component is passed the filteredSession to display the list of session

```typescript
export class SessionListComponent implements OnChanges {

  @Input() sessions?: ISession[]
  @Input() filterBy!: string
  filteredSession!:ISession[]

  //this method is auto called when any of the @Input value changes
  ngOnChanges(): void {
    //first we verify session exist in the first place
    
    if(this.sessions){

       if(this.filterBy==="all"){
            //this will clone a session, we dont want to touch the original session
            this.filteredSession = this.sessions.slice(0);
       }
       //filter, map.. these lambda expression also return brand new array, doen't change the original array
       else{
            this.filteredSession = this.sessions.filter(session => session.level.toLowerCase()===this.filterBy)
        }

     }
  }

  //template
  <div class="row" *ngFor="let session of filteredSession">
  //
  </div>

```

## Sorting 
similar to Firtering

```typescript
<button [class.active]="sortBy == 'vote'"(click)="sortBy = 'vote'" class="btn btn-default"> By Votes </button>
<button [class.active]="sortBy == 'name'"(click)="sortBy = 'name'"class="btn btn-default">By Name</button>
<app-session-list [sortBy]="sortBy" [filterBy]="filterBy" *ngIf="!IsAdd" [sessions]="event?.sessions"></app-session-list>
```

The componnet is same for both filtering and sorting : SessionList component

```typescript
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ISession } from 'src/IEvent';

@Component({
  selector: 'app-session-list',
  templateUrl: './session-list.component.html',
  styleUrls: ['./session-list.component.css']
})
export class SessionListComponent implements OnChanges {

  @Input() sessions?: ISession[]
  @Input() filterBy!: string
  @Input() sortBy!: string
  filteredSession!:ISession[]

  constructor() { }

  //this method is auto called when any of the @Input value changes
  ngOnChanges(): void {
    //first we verify session exist in the first place
    if(this.sessions){
      this.filterSession(this.filterBy,this.sessions)
      this.sortBy === 'name'? this.filteredSession.sort(sortByNameAsc): this.filteredSession.sort(sortByVoteDesc)
     }
  }

   filterSession(filter:string, session:ISession[]){
     if(this.filterBy==="all"){
            //this will clone a session, we dont want to touch the original session
            this.filteredSession = session.slice(0);
       }
       //filter, map.. these lambda expression also return brand new array, doen't change the original array
       else{
            this.filteredSession = session.filter(s => s.level.toLowerCase()===filter)
        }
   }

  ngOnInit(): void {
  }

}

function sortByNameAsc(s1: ISession, s2:ISession) {
  if(s1.name > s2.name) return 1
  else if(s1.name===s2.name) return 0
  else return -1
}

function sortByVoteDesc(s1: ISession, s2:ISession) {
  return s2.voters.length - s1.voters.length
}
```


## Angular's InjectionToken and @Inject() decorator

Sometimes we would want to use third party service like toastr for displaying notification like message. For using such service we need to register them to app.modules.ts provider. 

```typescript
declare let toastr: Toastr //expose global toastr object installed in our system
providers: [{provide:"canDeactivateCreate", useValue:checkDirty}, { provide: TOKEN_TOASTR, useValue:toastr }],
```

We'll have to create the TOKEN_TOASTR  as

```typescript
import { InjectionToken } from "@angular/core";

//we use InjectionToken to register our toastr service in angular's's DI 
export let TOKEN_TOASTR = new InjectionToken<Toastr>('toastr');

//this is just an iterface to let app know what sort of interface is exposed by toastr.
//Since this toastr interface is simple we are mocking it here for intelligence, else we will just give any as type.  
export interface Toastr{
   success (msg:string, title?: string):void;
   info (msg:string, title?: string):void;
   warning (msg:string, title?: string):void;
   error (msg:string, title?: string):void;
}
```

Now let's use it in Component. We'll have to use @Inject to inject the service in constructor 

  ``` typescript
  constructor(private auth: AuthService, private router: Router, @Inject(TOKEN_TOASTR) private toast : Toastr) { }
  this.toast.success(`User profile updated for ${userDate.firstName}`)
```








## TESTING ANGULAR 

#ATB : The angular Test BED

The Angular Test Bed (ATB) is a higher level Angular Only testing framework that allows us to easily test behaviours that depend on the Angular Framework.

We still write our tests in Jasmine and run using Karma but we now have a slightly easier way to create components, handle injection, test asynchronous behaviour and interact with our application.

**Configuration**

```typescript
import {TestBed, ComponentFixture} from '@angular/core/testing';
import {LoginComponent} from './login.component';
import {AuthService} from "./auth.service";

describe('Component: Login', () => {

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>; 
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [AuthService]
    });

    // create component and test fixture, with this method, the new component is created with the associated template
    //so we can test the template code as well like if the component loaded Header or button
    //If we create component with new, only class instance is created witout tempate
    fixture = TestBed.createComponent(LoginComponent); 

    // get test component from the fixture, create an instance of the component along with associated template
    component = fixture.debugElement.componentInstance; 

    // UserService provided to the TestBed, retrieves the AuthService  class and instantiate it if needed
    authService = TestBed.inject(AuthService); 

  });

    //spy is a feature of jasmine that let's you take on existing class, function or object abd mock it in
    //such a way that you can control what gets retyrned from the function call
    it('needsLogin returns true when the user has not been authenticated', () => {
    jasmine.createSpyObj(authService, 'isAuthenticated').and.returnValue(false);
    expect(component.needsLogin()).toBeTruthy();
    expect(authService.isAuthenticated).toHaveBeenCalled();
    });

    it('needsLogin returns false when the user has been authenticated', () => {
    jasmine.createSpyObj(authService, 'isAuthenticated').and.returnValue(true);
    expect(component.needsLogin()).toBeFalsy();
    expect(authService.isAuthenticated).toHaveBeenCalled();
    });

});
```

**Testing Change detection**

Trying to test whether changes in the state of our application trigger changes in the view without the Angular Test Bed is complicated. However with the ATB it’s much simpler.

Let's test Login component for change detection

```typescript
import { Component } from '@angular/core';
import { AuthService } from "./auth.service";

@Component({
  selector: 'app-login',
  template: `
  <a>
    <span *ngIf="needsLogin()">Login</span>
    <span *ngIf="!needsLogin()">Logout</span>
  </a>
`
})
export class LoginComponent {

  constructor(private auth: AuthService) {
  }

  needsLogin() {
    return !this.auth.isAuthenticated();
  }
}
```

Now let's write it's test spec


```typescript
describe('Component: Login', () => {

    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let authService: AuthService;
    let el: DebugElement;

    beforeEach(() => {

        // refine the test module by declaring the test component
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            providers: [AuthService]
        });

        // create component and test fixture
        fixture = TestBed.createComponent(LoginComponent);

        // get test component from the fixture
        component = fixture.componentInstance;

        // UserService provided to the TestBed
        authService = TestBed.get(AuthService);

        //  get the "a" element by CSS selector (e.g., by class name)
        el = fixture.debugElement.query(By.css('a'));
    });

    it('login button hidden when the user is authenticated', () => {
        // To being with Angular has not done any change detection so the content is blank.
        expect(el.nativeElement.textContent.trim()).toBe('');

        // Trigger change detection and this lets the template update to the initial value which is Login since by
        // default we are not authenticated
        fixture.detectChanges();
        expect(el.nativeElement.textContent.trim()).toBe('Login');

        // Change the authetication state to true
        spyOn(authService, 'isAuthenticated').and.returnValue(true);

        // The label is still Login! We need changeDetection to run and for angular to update the template.
        expect(el.nativeElement.textContent.trim()).toBe('Login');
        // Which we can trigger via fixture.detectChange()
        fixture.detectChanges();

        // Now the label is Logout
        expect(el.nativeElement.textContent.trim()).toBe('Logout');
    });
});
```

**Testing Async code**

Three ways:
1. jasmine's done function and spy callback - looks a bit complicated code
2. Angular async and whenStable  -- easier code to look then previous
3  ANgular fakeAsync and tick -- easiest of all but fakeAsync does have some drawbacks, it doesn’t track XHR requests for instance.

Async service

```typescript
export class AuthService {
    isAuthenticated(): Promise<boolean> {
        return Promise.resolve(!!localStorage.getItem('token'));
    }
}
```

LoginComponent that use asynch code from service

```typescript
import { Component } from '@angular/core';
import { AuthService } from "./auth.service";

@Component({
  selector: 'app-login',
  template: `
  <a>
    <span *ngIf="needsLogin">Login</span>
    <span *ngIf="!needsLogin">Logout</span>
  </a>
`
})
export class LoginComponent implements OnInit {

  needsLogin: boolean = true;

  constructor(private auth: AuthService) {
  }

  ngOnInit() {
    this.auth.isAuthenticated().then((authenticated) => {
      this.needsLogin = !authenticated;
    })
  }
}
```

Async code testing with all three methods

```typescript
/* tslint:disable:no-unused-variable */
import { TestBed, async, whenStable, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { AuthService } from "./auth.service";
import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";

describe('Component: Login', () => {

    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let authService: AuthService;
    let el: DebugElement;

    beforeEach(() => {

        // refine the test module by declaring the test component
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            providers: [AuthService]
        });

        // create component and test fixture
        fixture = TestBed.createComponent(LoginComponent);

        // get test component from the fixture
        component = fixture.componentInstance;

        // UserService provided to the TestBed
        authService = TestBed.get(AuthService);

        //  get the "a" element by CSS selector (e.g., by class name)
        el = fixture.debugElement.query(By.css('a'));
    });

    it('Button label via fakeAsync() and tick()', fakeAsync(() => {
        expect(el.nativeElement.textContent.trim()).toBe('');
        fixture.detectChanges();
        expect(el.nativeElement.textContent.trim()).toBe('Login');

        spyOn(authService, 'isAuthenticated').and.returnValue(Promise.resolve(true));

        component.ngOnInit();
        // Simulates the passage of time until all pending asynchronous activities complete
        tick();
        fixture.detectChanges();
        expect(el.nativeElement.textContent.trim()).toBe('Logout');
    }));

    it('Button label via async() and whenStable()', async(() => {
        // async() knows about all the pending promises defined in it's function body.
        fixture.detectChanges();
        expect(el.nativeElement.textContent.trim()).toBe('Login');
        spyOn(authService, 'isAuthenticated').and.returnValue(Promise.resolve(true));

        fixture.whenStable().then(() => {
            // This is called when ALL pending promises have been resolved
            fixture.detectChanges();
            expect(el.nativeElement.textContent.trim()).toBe('Logout');
        });

        component.ngOnInit();

    }));

    it('Button label via jasmine.done', (done) => {
        fixture.detectChanges();
        expect(el.nativeElement.textContent.trim()).toBe('Login');

        // Make the authService return a promise that resolves to true
        let spy = spyOn(authService, 'isAuthenticated').and.returnValue(Promise.resolve(true));
        // We trigger the component to check the authService again
        component.ngOnInit();

        // We now want to call a function when the Promise returned from authService.isAuthenticated() is resolved
        spy.calls.mostRecent().returnValue.then(() => {
            // The needsChanged boolean has been updated on the Component so to update the template we trigger change detection
            fixture.detectChanges();
            // Now the label is Logout
            expect(el.nativeElement.textContent.trim()).toBe('Logout');
            // We tell jasmine we are done with this test spec
            done();
        });
    });
});
```





# NgFundamentals

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
