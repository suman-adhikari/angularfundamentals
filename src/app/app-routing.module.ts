import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEventComponent } from './create-event/create-event.component';
import { CreateSessionComponent } from './create-session/create-session.component';
import { Error404Component } from './error/Error404';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventRouteService } from './event-route.service';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
  { path: 'events/new', component: CreateEventComponent, canDeactivate:['canDeactivateCreate']},
  { path: 'events', component: EventListComponent },
  { path: 'events/:id', component: EventDetailsComponent, canActivate:[EventRouteService] },
  { path: '', redirectTo: '/events', pathMatch: 'full' },
  { path: 'error', component: Error404Component },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  {path:'events/session/new', component:CreateSessionComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }