import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventThumbnailComponent } from './event-thumbnail/event-thumbnail.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { AppRoutingModule } from './app-routing.module';
import { CreateEventComponent } from './create-event/create-event.component';
import { Error404Component } from './error/Error404';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './auth.service';
import { CreateSessionComponent } from './create-session/create-session.component';
import { SessionListComponent } from './session-list/session-list.component';
import { Collapsable } from './common/collapsable.component';
import { DurationPipe } from './common/duration.pipe';
import { Toastr, TOKEN_TOASTR } from './common/toastr.service';

declare let toastr: Toastr

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    EventThumbnailComponent,
    NavBarComponent,
    EventDetailsComponent,
    CreateEventComponent,
    Error404Component,
    ProfileComponent,
    LoginComponent,
    CreateSessionComponent,
    SessionListComponent,
    Collapsable,
    DurationPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [{provide:"canDeactivateCreate", useValue:checkDirty}, { provide: TOKEN_TOASTR, useValue:toastr }],
  bootstrap: [AppComponent]
})
export class AppModule { }

 export function checkDirty(component:CreateEventComponent){
   if(component.isDirty){
     return window.confirm("you have not saved your form, do you want to leave?")
   }
  return true;
}