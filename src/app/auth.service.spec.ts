import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('login() should change user', () => {
    service.login("ram","ram123");
    expect(service.currentUser.firstName).toBe('ram')
    expect(service.currentUser.password).toBe('ram123')
  });

   it('isAuthenticated should return boolean', () => {
    expect(typeof(service.isAuthenticated())).toBe("boolean")
  });

  it('updateProfile() should change user', () => {
    service.updateProfile("ram","hawa");
    expect(service.currentUser.firstName).toBe('ram')
    //expect(service.currentUser.lastName).toBe('hawa')
  });

});
