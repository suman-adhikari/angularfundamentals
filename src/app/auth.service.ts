import { Injectable } from '@angular/core';
import { IUser } from './IUser';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  currentUser!:IUser

  constructor() { }

  login(username:string,password:string):void{
      this.currentUser={
        id:1,
        firstName:username,
        lastName:"Brown",
        password:password,
        userName:username
      }
  }

  isAuthenticated():boolean{
    return !!this.currentUser;
  }

  updateProfile(firstname:string,lastname:string):void{
    this.currentUser.firstName=firstname;
    this.currentUser.lastName=lastname
  }

}
