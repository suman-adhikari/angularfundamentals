import { Component, Input } from '@angular/core';


@Component({
  selector:'app-collapsable',
  template: `<div class="well pointer" (click)="toggle()">
            <h4>
               <ng-content select="[well-title]" ></ng-content>
            </h4>
            <ng-content  select="[well-body]" *ngIf="visible"> </ng-content>
           </div> `,

   styles:[ ' .pointer{ cursor:pointer } ' ]
})
export class Collapsable  {
  visible:boolean = false;

  toggle(){
       this.visible = ! this.visible;
   }

}
