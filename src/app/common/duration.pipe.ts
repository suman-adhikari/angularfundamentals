import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name:'duration'})
export class DurationPipe implements PipeTransform{

  //transform takes numeric value and returns striing
  transform(value:number): string{
      switch(value){
       case 1 : return 'Half hour';
       case 2 : return 'One hour';
       case 3 : return 'One and half hour';
       case 4 : return 'Two hour';
       default: return value.toString();

     }
  }

}