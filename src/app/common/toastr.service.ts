import { InjectionToken } from "@angular/core";

export let TOKEN_TOASTR = new InjectionToken<Toastr>('toastr');

export interface Toastr{
   success (msg:string, title?: string):void;
   info (msg:string, title?: string):void;
   warning (msg:string, title?: string):void;
   error (msg:string, title?: string):void;
}