import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IEvent } from 'src/IEvent';
import { EventsService } from '../events.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  isDirty:boolean = true;

  newEvent!:any

  constructor(private router: Router,private eventService: EventsService) { }

  ngOnInit(): void {
  }

  cancel() {
    this.router.navigate(['/events'])
  }

  saveEvent(formData:IEvent){
     this.eventService.saveEvent(formData)
     this.isDirty=false;
     this.router.navigate(['/events'])
  }

}
