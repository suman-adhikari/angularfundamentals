import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ISession } from 'src/IEvent';

@Component({
  selector: 'app-create-session',
  templateUrl: './create-session.component.html',
  styleUrls: ['./create-session.component.css']
})
export class CreateSessionComponent implements OnInit {

  @Output() SaveSessionEvent = new EventEmitter();
  @Output() CancelSessionEvent = new EventEmitter();

  sessionFormGroup!: FormGroup;
  name!: FormControl;
  presenter!:FormControl;
  duration!: FormControl;
  level!: FormControl;
  abstract!:FormControl
  

  constructor() { }

  ngOnInit(): void {

    this.name = new FormControl('', Validators.required);
    this.presenter = new FormControl('', Validators.required);
    this.duration = new FormControl('', Validators.required);
    this.level = new FormControl('', Validators.required);
    this.abstract = new FormControl('', [Validators.required, Validators.minLength(10), this.restrictedWords(['foo','bar'])]);

    this.sessionFormGroup = new FormGroup({
       name: this.name,
       presenter:this.presenter,
       duration:this.duration,
       level: this.level,
       abstract: this.abstract
     })
     
  }

   SaveSession(formValue:any){
     let session: ISession = {
       id: 0,
       name: formValue.name,
       presenter: formValue.presenter,
       level: formValue.level,
       duration: formValue.duration,
       abstract:formValue.abstract,
       voters:[]
     }

    console.log(session)

    this.SaveSessionEvent.emit(session)
   
   }

  // custom validation
   private restrictedWords(words:string[]){
       return (control:FormControl) =>{
           if(!words) return null;

          var invalidWords = words.map(w=> control.value.includes(w)? w : null)
                             .filter(w=>w!= null)

            return invalidWords && invalidWords.length>0 ? {'restrictedWords':invalidWords.join(', ')}:null
        }
   }

  cancelAddSession(){
     this.CancelSessionEvent.emit()
  }



}
