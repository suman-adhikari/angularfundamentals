import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IEvent, ISession } from 'src/IEvent';
import { EventsService } from '../events.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {

  event!: IEvent;
  IsAdd:boolean=false;
  filterBy:string='all'
  sortBy:string ='vote'
  

  constructor(private  eventService: EventsService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.getEventDetail()
  }

  getEventDetail(): void {
    let id = Number(this.route.snapshot.paramMap.get('id'));
    this.event = this.eventService.getEventById(id)!;
  }

  Back() {
    this.location.back();
  }

  addNewSession(){
    this.IsAdd= true;
 }

  SaveSession(session:ISession){
      const maxId = Math.max.apply(null,this.event?.sessions.map(s => s.id))
      session.id = maxId + 1;
      this.event.sessions.push(session);
      this.eventService.updateEvent(this.event);
      this.IsAdd = false;
   }

  CancelSessionEvent(){
    this.IsAdd = false;
  }


}
