import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToastrService } from 'ngx-toastr';
import { IEvent } from 'src/IEvent';
import { EventsService } from '../events.service';

import { EventListComponent } from './event-list.component';

describe('EventListComponent', () => {
  let component: EventListComponent;
  let fixture: ComponentFixture<EventListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EventListComponent],
      providers:[{provide:EventsService, useValue:EventsServiceStub},{provide:ToastrService, useValue:ToastrServiceStub}]
    })
      .compileComponents();
  });

  fixture = TestBed.createComponent(EventListComponent);
  const comp = fixture.componentInstance;


  
  let EventsServiceStub = function(){}
  let ToastrServiceStub = function(){}
   


  beforeEach(() => {
    fixture = TestBed.createComponent(EventListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
