import { Component, Inject, OnInit } from '@angular/core';
import { EventsService } from '../events.service';
import { ToastrService } from 'ngx-toastr';
import { IEvent } from 'src/IEvent';
import { Toastr, TOKEN_TOASTR } from '../common/toastr.service';


@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  AllEvent!: IEvent[];


  constructor(private eventService: EventsService, @Inject(TOKEN_TOASTR) private toastr: Toastr) { }

  ngOnInit(): void {
    this.AllEvent = this.eventService.getEvents();
  }

  handleClick(eventName: string): void {
    this.toastr.success("WOW")
  }


}