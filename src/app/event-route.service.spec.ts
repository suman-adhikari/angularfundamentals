import { Router } from "@angular/router";
import { EventRouteService } from "./event-route.service";
import { EventsService } from "./events.service"

describe('EventRouteService',()=>{

    let eventRouteService : EventRouteService;
    let route: Router;

    beforeEach(()=> {
        route = jasmine.createSpyObj("Router", ['navigate']);
        eventRouteService = new EventRouteService(route, new EventsService());
    });

    let route1 = jasmine.createSpyObj('Route', ['']);
      route1.params = {
      id: '1'
     }

     let route21 = jasmine.createSpyObj('Route', ['']);
      route21.params = {
      id: '21'
     }

    it('canActivate(1) should return true',()=>{
        expect(eventRouteService.canActivate(route1)).toBe(true);
    });

     it('canActivate(21) should navigate to \error',()=>{
        eventRouteService.canActivate(route21);
        expect(route.navigate).toHaveBeenCalledWith(['/error']);
    });

 

    

})

