import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { EventsService } from './events.service';


@Injectable({
  providedIn: 'root'
})

export class EventRouteService implements CanActivate {

  constructor(private router: Router, private eventService: EventsService) { 

  }

  //we use this canActivate in the router where we want to take user to error page is no detail page is found for that id
  canActivate(route: ActivatedRouteSnapshot) {
 
    console.log(route)
    //!! converts  the statement to boolean
    const eventExist = !!this.eventService.getEventById(+route.params["id"])
    if (!eventExist)
      this.router.navigate(['/error'])
    return eventExist

  }

}
