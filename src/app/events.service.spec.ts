import { events } from "src/Events";
import { EventsService } from "./events.service"

describe('EventService',()=>{

    let service : EventsService;
    let mockevent : any;
    let newevent : any;

    beforeEach(()=> {service= new EventsService();} );

    newevent={id:3, name:"mockevent"}

     it('getEventById(1) should have name Angular Connect and id=1',()=>{
        expect(service.getEventById(1)?.name).toBe("Angular Connect");
        expect(service.getEventById(1)?.id).toBe(1);
    });

     it('getEvent() should return array of IEvent',()=>{
        expect(service.getEvents()).toBe(events);
        expect(typeof(service.getEvents())).toBe(typeof(events));
    });

     it('saveEVents() should return aray lenth 6',()=>{
        service.saveEvent(newevent)
        expect(events.length).toBe(6);
    });

    

})