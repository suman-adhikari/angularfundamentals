import { Injectable } from '@angular/core';
import { events } from 'src/Events';
import { IEvent } from 'src/IEvent';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor() { }

  getEvents():IEvent[] {
    return events
  }

  saveEvent(evnt:any){
    evnt.id=23;
    evnt.sessions=[]
    events.push(evnt);
  }

  updateEvent(evnt:IEvent){
     let index = events.findIndex(x=> x.id = evnt.id)
     events[index] = evnt;
  }

  getEventById(id: number) {
    return events.find(e => e.id === id);
  }



}
