import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userName!:string
  password!:string

  constructor(private auth:AuthService, private route: Router) { }

  login(formvalues:any){
    this.auth.login(formvalues.userName,formvalues.password);
    this.route.navigate(["/events"]);
  }

  ngOnInit(): void {
  }

}
