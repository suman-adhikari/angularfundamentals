import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Toastr, TOKEN_TOASTR } from '../common/toastr.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  firstName!:FormControl;
  lastName!:FormControl;
  profileForm!: FormGroup;

  constructor(private auth: AuthService, private router: Router, @Inject(TOKEN_TOASTR) private toast : Toastr) { }

  ngOnInit(): void {
    this.firstName = new FormControl(this.auth.currentUser.firstName,[Validators.required, Validators.pattern('[a-zA-Z].*')]);
    this.lastName = new FormControl(this.auth.currentUser.lastName,Validators.required);

    //profileForm this variable should match with template's formGroup ie [formGroup]="profileForm"
    //the key of FormGroup should match with template's formControlName ie formControlName="firstName", formControlName="lastName"
    this.profileForm = new FormGroup({
      firstName:this.firstName,
      lastName:this.lastName
    })

  }  

  updateProfile(userDate:any){
    
    if(this.profileForm.valid){
    this.auth.updateProfile(userDate.firstName, userDate.lastName)
    //this.router.navigate(['/events'])
    this.toast.success(`User profile updated for ${userDate.firstName}`)
    }


  }

  isRequired(formElement:any):boolean{
     return formElement.errors.required;
   }

   isValidPattern(formElement:any):boolean{
     return formElement.errors.pattern;
   }

  isValidFname():boolean{
    console.log(this.firstName)
    return this.firstName.valid
  }

  isValidLname():boolean{
    return this.lastName.valid
  }

  isValid(){
    return this.firstName.valid && this.lastName.valid
  }

}
