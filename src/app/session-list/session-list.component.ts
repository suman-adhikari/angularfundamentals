import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ISession } from 'src/IEvent';

@Component({
  selector: 'app-session-list',
  templateUrl: './session-list.component.html',
  styleUrls: ['./session-list.component.css']
})
export class SessionListComponent implements OnChanges {

  @Input() sessions?: ISession[]
  @Input() filterBy!: string
  @Input() sortBy!: string
  filteredSession!:ISession[]

  constructor() { }

  //this method is auto called when any of the @Input value changes
  ngOnChanges(): void {
    //first we verify session exist in the first place
    if(this.sessions){
      this.filterSession(this.filterBy,this.sessions)
      this.sortBy === 'name'? this.filteredSession.sort(sortByNameAsc): this.filteredSession.sort(sortByVoteDesc)
     }
  }

   filterSession(filter:string, session:ISession[]){
     if(this.filterBy==="all"){
            //this will clone a session, we dont want to touch the original session
            this.filteredSession = session.slice(0);
       }
       //filter, map.. these lambda expression also return brand new array, doen't change the original array
       else{
            this.filteredSession = session.filter(s => s.level.toLowerCase()===filter)
        }
   }

  ngOnInit(): void {
  }

}

function sortByNameAsc(s1: ISession, s2:ISession) {
  if(s1.name > s2.name) return 1
  else if(s1.name===s2.name) return 0
  else return -1
}

function sortByVoteDesc(s1: ISession, s2:ISession) {
  return s2.voters.length - s1.voters.length
}

